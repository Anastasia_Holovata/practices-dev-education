package com.github.practice2;

public class Main {
    public static void main(String[] args) {
        Person p = new Person(1L, "Anastasia", "Holovata", 18, "Dnepr");
        String jsonStr = toJsonFormat(p);
        System.out.println("Json Person:\n" + jsonStr);

        String xmlStr = toXmlFormat(p);
        System.out.println("Xml person:\n" + xmlStr);

        String yamlStr = toYamlFormat(p);
        System.out.println("Yaml person:\n" + yamlStr);

        String csvStr =  toCsvFormat(p);
        System.out.println("Csv Person:\n" + csvStr);
    }

    public static String toJsonFormat(Person p) {
        return "[\n\n" + " {\n\t\"id\":\"" + p.getId() + "\",\n\t\"firstName\":\"" + p.getFirstName()
                + "\",\n\t\"lastName\":\"" + p.getLastName() + "\",\n\t\"age\":\"" + p.getAge() + "\",\n\t\"city\":\""
                + p.getCity() + "\"\n" + " }\n\n]\n";
    }

    public static String toXmlFormat(Person p) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<people>\n  <person>\n\t<id>" + p.getId()
                + "</id>\n\t<firstName>" + p.getFirstName() + "</firstName>\n\t<lastName>" + p.getLastName()
                + "</lastName>\n\t<age>" + p.getAge() + "</age>\n\t<city>" + p.getCity()
                + "</city>\n  </person>\n</people>\n";
    }

    public static String toYamlFormat(Person p) {
        return "Person:\n\t-id: " + p.getId() + "\n\t-firstName: " + p.getFirstName() + "\n\t-lastName: "
                + p.getLastName() + "\n\t-age: " + p.getAge() + "\n\t-city: " + p.getCity() + "\n";
    }

    public static String toCsvFormat(Person p) {
        return p.getId() + "," + p.getFirstName() + "," + p.getLastName() + "," + p.getAge() + "," + p.getCity() + "\n";
    }

//    public static String toBinFormat(Person p) {
//        return "";
//    }
}
