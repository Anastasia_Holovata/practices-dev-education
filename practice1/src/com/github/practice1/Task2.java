package com.github.practice1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Даны 4 числа типа int. Сравнить их и вывести наименьшее на консоль.
 * Вывести на консоль количество максимальных чисел среди этих четырех.
 */
public class Task2 {

    private static int a;
    private static int b;
    private static int c;
    private static int d;

    private static int[] array;

    public static void main(String[] args) {
        inputNumbers();
        numbersToArray();
        minimumOfNumbers(array);
        comparisonNumbers(array);
    }

    private static int getIntValueFromInput() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            return Integer.parseInt(reader.readLine());
        } catch (IOException | NumberFormatException e) {
            throw new IllegalArgumentException("You input incorrect value!");
        }
    }

    private static void inputNumbers() {
        System.out.print("Enter A: ");
        a = getIntValueFromInput();

        System.out.print("Enter B: ");
        b = getIntValueFromInput();

        System.out.print("Enter C: ");
        c = getIntValueFromInput();

        System.out.print("Enter D: ");
        d = getIntValueFromInput();

        System.out.println("We got numbers: " + a + ", "+ b + ", " + c + ", " + d + ".");
    }

    private static void numbersToArray() {
        array = new int[] {a, b, c, d};
        System.out.println(Arrays.toString(array));
    }

    private static void minimumOfNumbers(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int min_i = i;

            for (int j = i+1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    min_i = j;
                }
            }
            if (i != min_i) {
                int tmp = array[i];
                array[i] = array[min_i];
                array[min_i] = tmp;
            }
        }
        System.out.println("Sorted array: " + Arrays.toString(array));
        System.out.println("Minimum element equals " + array[0]);
    }
    private static void comparisonNumbers(int[] array) {
        int counterOfMaxValues = 1;
        for (int i = array.length-1; i > 0; i--) {
            if (array[i] == array[i-1]) {
                counterOfMaxValues++;
            }
        }
        if (counterOfMaxValues >= 1) {
            System.out.println("There are " + counterOfMaxValues + " maximum values!");
        }
    }
}

