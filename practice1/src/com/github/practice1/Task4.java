package com.github.practice1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Даны имена 2х человек (тип String). Если имена равны, то вывести сообщение о том, что люди являются тезками.
 */
public class Task4 {

    private static String name1;
    private static String name2;

    public static void main(String[] args) {
        System.out.println(equalityOfNames());
    }

    private static String getStringValueFromInput() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new IllegalArgumentException("You input incorrect value!");
        }
    }

    private static void inputNames() {
        System.out.print("Name 1 - ");
        name1 = getStringValueFromInput();

        System.out.print("Name 2 - ");
        name2 = getStringValueFromInput();
    }

    private static boolean equalityOfNames() {
        inputNames();
        if (name1.equalsIgnoreCase(name2)) {
            System.out.println("There are the namesakes!");
            return true;
        }
        else {
            System.out.println("The names are different!");
            return false; }
    }
}
