package com.github.practice1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Даны 5 чисел (тип int). Вывести вначале наименьшее, а затем наибольшее из данных чисел.
 */
public class Task3 {

    private static int a;
    private static int b;
    private static int c;
    private static int d;
    private static int e;
    private static int[] array;

    public static void main(String[] args) {
        inputNumbers();
        numbersToArray();
        minAndMaxValue(array);
    }

    private static int getIntValueFromInput() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            return Integer.parseInt(reader.readLine());
        } catch (IOException | NumberFormatException e) {
            throw new IllegalArgumentException("You input incorrect value!");
        }
    }

    private static void inputNumbers() {
        System.out.print("Enter A: ");
        a = getIntValueFromInput();

        System.out.print("Enter B: ");
        b = getIntValueFromInput();

        System.out.print("Enter C: ");
        c = getIntValueFromInput();

        System.out.print("Enter D: ");
        d = getIntValueFromInput();

        System.out.print("Enter E: ");
        e = getIntValueFromInput();

        System.out.println("We got numbers: " + a + ", " + b + ", " + c + ", " + d + ", " + e + ".");
    }

    private static void numbersToArray() {
        array = new int[]{a, b, c, d, e};
    }

    private static void minAndMaxValue(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int min_i = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    min_i = j;
                }
            }
            if (i != min_i) {
                int tmp = array[i];
                array[i] = array[min_i];
                array[min_i] = tmp;
            }
        }
        System.out.println("Sorted array: " + Arrays.toString(array));
        System.out.println("Minimum element equals " + array[0]);
        System.out.println("Maximum element equals " + array[array.length - 1]);
    }
}

