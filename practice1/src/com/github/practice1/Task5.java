package com.github.practice1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Дано число месяца (тип int). Необходимо определить время года (зима, весна, лето, осень) и вывести на консоль.
 */
public class Task5 {

    private static int numberOfMonth;

    public static void main(String[] args) {

        System.out.println(getPartOfTheYear());
    }

    private static int getIntValueFromInput() {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            return Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            throw new IllegalArgumentException("You input incorrect value!");
        }
    }

    private static String getPartOfTheYear() {

        System.out.print("Enter the number of month: ");
        numberOfMonth = getIntValueFromInput();

        if (numberOfMonth >= 1 && numberOfMonth < 3 || numberOfMonth == 12) {
            return "Winter";
        } else if (numberOfMonth >= 3 && numberOfMonth < 6) {
            return "Spring";
        } else if (numberOfMonth >= 6 && numberOfMonth < 9) {
            return "Summer";
        } else if (numberOfMonth >= 9 && numberOfMonth < 12) {
            return "Autumn";
        } else {
            return "You entered wrong number of month!";
        }
    }

}
