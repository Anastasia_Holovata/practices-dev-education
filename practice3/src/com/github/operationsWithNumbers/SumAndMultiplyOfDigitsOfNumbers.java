package com.github.operationsWithNumbers;

/**
 * Сумма и произведение цифр числа
 */
public class SumAndMultiplyOfDigitsOfNumbers {

    public static void main(String[] args) {
        System.out.println("The sum of " + 1 + " is " + getSumOfDigits(1) +
                ";\nthe multiply is " + getMultiplyOfDigits(1) + ".");
        System.out.println("The sum of " + 0 + " is " + getSumOfDigits(0) +
                ";\nthe multiply is " + getMultiplyOfDigits(0) + ".");
        System.out.println("The sum of " + 110 + " is " + getSumOfDigits(110) +
                ";\nthe multiply is " + getMultiplyOfDigits(110) + ".");
        System.out.println("The sum of " + 365 + " is " + getSumOfDigits(365) +
                ";\nthe multiply is " + getMultiplyOfDigits(365) + ".");
    }

    private static int getSumOfDigits(int number) {
        int sum = 0;
        String stringNumber = String.valueOf(number);
        char[] charArray = stringNumber.toCharArray();
        for (int i = 0; i < charArray.length; i++) {
            sum += Character.getNumericValue(charArray[i]);
        }
        return sum;
    }

    private static int getMultiplyOfDigits(int number) {
        int multiply = 1;
        String stringNumber = String.valueOf(number);
        char[] charArray = stringNumber.toCharArray();

        for (int i = 0; i < charArray.length; i++) {
            multiply *= Character.getNumericValue(charArray[i]);
        }
        return multiply;
    }
}
