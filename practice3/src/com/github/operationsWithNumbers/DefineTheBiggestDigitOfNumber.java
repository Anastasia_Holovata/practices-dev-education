package com.github.operationsWithNumbers;

/**
 * Определить, какая цифра числа больше
 */
public class DefineTheBiggestDigitOfNumber {
    public static void main(String[] args) {
        System.out.println(getTheBiggestDigit(1));
        System.out.println(getTheBiggestDigit(8));
        System.out.println(getTheBiggestDigit(928));
        System.out.println(getTheBiggestDigit(28));
        System.out.println(getTheBiggestDigit(5432));
    }

    private static int getTheBiggestDigit(int number) {
        String stringNumber = String.valueOf(number);
        char[] charArray = stringNumber.toCharArray();
        int biggestValue = charArray[0];
        int prev = -9;
        if (charArray.length > 1) {
            for (int i = 0; i < charArray.length; i++) {
                biggestValue = charArray[i];
                if (biggestValue >= prev) {
                    prev = charArray[i];
                    biggestValue = Character.getNumericValue(biggestValue);
                }
                else { biggestValue = Character.getNumericValue(prev); }
            }
        }
        else { biggestValue = Character.getNumericValue(charArray[0]); }

        return biggestValue;
    }
}
