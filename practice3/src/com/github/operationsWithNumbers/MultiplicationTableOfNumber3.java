package com.github.operationsWithNumbers;

/**
 * Таблица умножения на 3
 */
public class MultiplicationTableOfNumber3 {
    public static void main(String[] args) {
        multiplication(3);
    }

    private static void multiplication(int number) {
        int multiply = 1;
        for (int i = 1; i <= 10; i++) {
            multiply = number * i;
            System.out.println(number + " x " + i + " = " + multiply);
        }
    }
}
