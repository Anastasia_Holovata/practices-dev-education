package com.github.operationsWithNumbers;

/**
 * Числа Фибоначчи первые 11
 */
public class FibonacciNumbers {
    public static void main(String[] args) {
        getFibonacciRow();
    }

    private static void getFibonacciRow() {
        System.out.print("Fibonacci row is ");

        int element1 = 0;
        int element2 = 1;
        int[] arrayFibonacci = new int[11];
        arrayFibonacci[0] = element1;
        arrayFibonacci[1] = element2;

        for (int i = 2; i < arrayFibonacci.length; i++) {
            arrayFibonacci[i] = element1 + element2;
            element1 = element2;
            element2 = arrayFibonacci[i];
        }
        for (int i = 0; i < arrayFibonacci.length; i++) {
            System.out.print(arrayFibonacci[i] + " ");
        }
    }
}
