package com.github.cyclesFor;

public class CyclesFor {
    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
    }

    /**
     * While i=0 … i=-10;
     */

    private static void task1() {

        System.out.println("========Task1========\n");

        for (int i = 0; i >= -10; i--) {
            System.out.print(i + " ");
        }
    }

    /**
     * While i=-20 … i=20;
     */
    private static void task2() {

        System.out.println("\n\n========Task2========\n");

        for (int i = -20; i <= 20; i++) {
            System.out.print(i + " ");
        }
    }

    /**
     * While i=30 … i=10;
     */
    private static void task3() {

        System.out.println("\n\n========Task3========\n");

        for (int i = 30; i >= 10; i--) {
            System.out.print(i + " ");
        }
    }

    /**
     * While i=1 … i=100 кратное семи;
     */
    private static void task4() {

        System.out.println("\n\n========Task4========\n");

        for (int i = 1; i <= 100; i++) {
            if (i % 7 == 0) {
                System.out.print(i + " ");
            }
        }
    }

    /**
     * While i=1 … i=100 кратное 3 и 5;
     */
    private static void task5() {
        System.out.println("\n\n========Task5========\n");
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print(i + " ");
            }
        }
    }
}
