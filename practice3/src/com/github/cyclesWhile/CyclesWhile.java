package com.github.cyclesWhile;

public class CyclesWhile {
    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
    }

    /**
     * While i=0 … i=-10;
     */
    private static void task1() {

        System.out.println("========Task1========");

        int i = 0;
        while(i >= -10) {
            System.out.print(i + " ");
            i--;
        }
    }

    /**
     * While i=-20 … i=20;
     */
    private static void task2() {

        System.out.println("\n\n========Task2========");

        int i = -20;
        while(i <= 20) {
            System.out.print(i + " ");
            i++;
        }
    }

    /**
     * While i=30 … i=10;
     */
    private static void task3() {

        System.out.println("\n\n========Task3========");

        int i = 30;
        while(i >= 10) {
            System.out.print(i + " ");
            i--;
        }
    }

    /**
     * While i=1 … i=100 кратное семи;
     */
    private static void task4() {

        System.out.println("\n\n========Task4========");

        int i = 1;
        while(i <= 100) {
            if (i % 7 == 0) {
                System.out.print(i + " ");
            }
            i++;
        }
    }

    /**
     * While i=1 … i=100 кратное 3 и 5;
     */
    private static void task5() {
        System.out.println("\n\n========Task5========");

        int i = 1;
        while(i <= 100) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.print(i + " ");
            }
            i++;
        }
    }
}
