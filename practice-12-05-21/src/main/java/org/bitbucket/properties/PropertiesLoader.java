package org.bitbucket.properties;

import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class PropertiesLoader {

    public static Map<String, Object> getProperties(String filePropertyFile){
        Properties getProperties = new Properties();
        FileInputStream inputStream = null;
        HashMap<String, Object> propertyMap = new HashMap<String, Object>();
        try {
            inputStream = new FileInputStream(filePropertyFile);
            getProperties.load(inputStream);
            propertyMap.putAll((Map) getProperties);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return propertyMap;
    }
}
