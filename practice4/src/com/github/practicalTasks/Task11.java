package com.github.practicalTasks;

/**
 * 11. Расставить пробелы после всех знаков препинания (, . : ? !)
 */
public class Task11 {

    private final static String line = "В лесу родилась елочка,В лесу она росла!Зимой и летом стройная?Зеленая была.";

    public static void main(String[] args) {
        System.out.println("Line before processing:");
        System.out.println(line);
        System.out.println("Line after processing:");
        System.out.println(putSpacesAfterPunctuation(line));
    }

    public static String putSpacesAfterPunctuation(String line) {
        return line.replaceAll("(\\.|,|:|\\?|!)(?!$)", "$0 ");
    }
}
