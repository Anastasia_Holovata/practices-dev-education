package com.github.practicalTasks;

/**
 * 12. Все символы строки (пути к файлу) c / перевернуть \\
 */
public class Task12 {
    public static void main(String[] args) {
        String testFilePath = "C:/Users/User/MyProjects/MyProjects.iml";
        System.out.println("The path before change: " + testFilePath);
        System.out.println("The path after change: " + getChangedPathToFile(testFilePath));
    }

    public static String getChangedPathToFile(String line) {
        return line.replaceAll("/", "\\\\\\\\");
    }
}
