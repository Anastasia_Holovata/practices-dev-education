package com.github.practicalTasks;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 13. Указать путь к файлу и провалидировать его (Pattern, Matcher) (на недопустимые символы)
 */
public class Task13 {

    private final static String REMOTE_LOCATION_WIN_PATTERN = "([a-zA-Z]:)?(\\\\[a-z  A-Z0-9_.-]+)+.(txt|gif|jpg|png|jpeg|pdf|doc|docx|xls|xlsx|DMS)\\\\?";

    private final static String REMOTE_LOCATION_LINUX_PATTERN = "^(/[^/]*)+.(txt|gif|jpg|png|jpeg|pdf|doc|docx|xls|xlsx|DMS)/?$";

    private final static Pattern linux_pattern = Pattern.compile(REMOTE_LOCATION_LINUX_PATTERN);
    private final static Pattern win_pattern = Pattern.compile(REMOTE_LOCATION_WIN_PATTERN);

    private static final boolean WINDOWS = System.getProperty("os.name").startsWith("Windows");

    public static void main(String[] args) {
        //System.out.println(isVali("C:\\\\Users\\\\User\\\\MyProjects\\\\MyProjects.iml"));
        //System.out.println(isPathValid("C:\\\\Users\\\\User\\\\MyProjects\\\\afuhjfs?"));
        System.out.println();
    }

    public boolean isValidDirectory(String path) {
        Pattern pathDirectoryPattern = Pattern.compile("^/|(/[a-zA-Z0-9_-]+)+$");
        return path != null && !path.trim().isEmpty() && pathDirectoryPattern.matcher( path ).matches();
    }

    public boolean checkPathValidity(String filePath) {
        Matcher m = WINDOWS ? win_pattern.matcher(filePath) : linux_pattern.matcher(filePath);

        return m.matches();
    }


//    public static boolean isPathValid(String path) {
//
//        try {
//            Paths.get(path);
//
//        } catch (InvalidPathException ex) {
//            return false;
//        }
//
//        return true;
//    }
//    public static boolean isValidFilePath(String path) {
//        File f = new File(path);
//        try {
//            f.getCanonicalPath();
//            return true;
//        }
//        catch (IOException e) {
//            return false;
//        }
//    }
}
