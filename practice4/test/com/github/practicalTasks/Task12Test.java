package com.github.practicalTasks;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)

public class Task12Test {

    private String expectedString;
    private String actualString;

    public Task12Test(String expectedString, String actualString) {
        this.expectedString = expectedString;
        this.actualString = actualString;
    }
    @Test
    public void getChangedPathToFile() {
        assertEquals(expectedString,
                Task12.getChangedPathToFile(actualString));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new String[][] {
                { "C:\\\\Users\\\\User\\\\MyProjects\\\\MyProjects.iml",
                        "C:/Users/User/MyProjects/MyProjects.iml" },
                { "C:\\\\test\\\\fiksiki\\\\com\\\\github\\\\file\\\\crud\\\\app\\\\conventer\\\\jsonExample.json\\\\",
                        "C:/test/fiksiki/com/github/file/crud/app/conventer/jsonExample.json/" },
                { "\\\\\\\\", "//" }
        });
    }
}