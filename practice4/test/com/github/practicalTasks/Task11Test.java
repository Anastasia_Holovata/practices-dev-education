package com.github.practicalTasks;

import com.github.practicalTasks.Task11;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class Task11Test {

    private String expectedString;
    private String actualString;

    public Task11Test(String expectedString, String actualString) {
        this.expectedString = expectedString;
        this.actualString = actualString;
    }

    @org.junit.Test
    public void putSpacesAfterPunctuation() {
        assertEquals(expectedString,
                Task11.putSpacesAfterPunctuation(actualString));
    }

    @Parameterized.Parameters
    public static Collection testDataProvider() {
        return Arrays.asList(new String[][] {
                { "В лесу родилась елочка, В лесу она росла! Зимой и летом стройная? Зеленая была.",
                        "В лесу родилась елочка,В лесу она росла!Зимой и летом стройная?Зеленая была." },
                { "Мы едем, едем,  едем,  В далекие края. Счастливые / соседи,  Счастливые: друзья", "Мы едем,едем, едем, В далекие края.Счастливые / соседи, Счастливые:друзья" },
                { "null! ", "null!" }
        });
    }
}