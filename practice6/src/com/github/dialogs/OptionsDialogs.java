package com.github.dialogs;

import javax.swing.*;
import java.awt.*;

class OptionsDialogs {

    static void errorMessage(JFrame frame) {
        JOptionPane.showMessageDialog(frame, "Error message.",
                "Warning", JOptionPane.ERROR_MESSAGE);
    }

    static Color colorDialog(JFrame frame) {
        return JColorChooser.showDialog(frame, "Select a color", Color.RED);
    }

    static int chooseFile() {
        JFileChooser fileOpen = new JFileChooser();
        return fileOpen.showDialog(null, "Открыть файл");
    }

    static int saveFile() {
        JFileChooser fileSave = new JFileChooser();
        return fileSave.showSaveDialog(null);
    }
}
