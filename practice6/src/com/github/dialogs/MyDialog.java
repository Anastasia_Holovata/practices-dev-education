package com.github.dialogs;

import javax.swing.*;

class MyDialog extends JDialog {
    MyDialog() {
        String[] choices = {"A", "B", "C", "D", "E", "F"};
        String input = (String) JOptionPane.showInputDialog(null, "Choose now...",
                "The Choice of a Lifetime", JOptionPane.QUESTION_MESSAGE, null, choices, choices[1]);
        System.out.println(input);
    }
}
