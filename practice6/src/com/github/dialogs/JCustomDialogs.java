package com.github.dialogs;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;

class JCustomDialogs extends JFrame {
    JCustomDialogs() throws HeadlessException {
        setSize(400, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JButton btnErrorMessage = new JButton("Error message");
        JButton btnChooseColor = new JButton("Color");
        JButton btnFileChooser = new JButton("File Chooser");
        JButton btnSaveFile = new JButton("Save File");
        JButton btnMyDialog = new JButton("My Dialog");


        btnErrorMessage.setBounds(10, 10, 120, 30);
        btnChooseColor.setBounds(10, 60, 120, 30);
        btnFileChooser.setBounds(10, 110, 120, 30);
        btnSaveFile.setBounds(10, 160, 120, 30);
        btnMyDialog.setBounds(10, 210, 120, 30);

        btnErrorMessage.addActionListener(e -> OptionsDialogs.errorMessage(this));
        btnChooseColor.addActionListener(e -> {
            Color color = OptionsDialogs.colorDialog(this);
            System.out.println(color);
        });
        btnFileChooser.addActionListener(e -> {
            JFileChooser f = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            int r = f.showOpenDialog(null);
            OptionsDialogs.chooseFile();
        });
        btnSaveFile.addActionListener(e -> {
            JFileChooser f = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            int r = f.showOpenDialog(null);
            OptionsDialogs.saveFile();
        });
        btnMyDialog.addActionListener(e -> {
            MyDialog myDialog = new MyDialog();
            myDialog.setVisible(true);
        });

        add(btnFileChooser);
        add(btnChooseColor);
        add(btnErrorMessage);
        add(btnSaveFile);
        add(btnMyDialog);

        setLayout(null);
        setVisible(Boolean.TRUE);
    }
}
