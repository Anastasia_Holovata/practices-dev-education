package com.github.practice5.customerPart;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

public class CustomerDaoTest {

    private static List<Customer> expectedList = new ArrayList<>();

    @org.junit.Test
    public void customersNamesakes() {
        expectedList.add(CustomerList.fillTheList().get(1));
        expectedList.add(CustomerList.fillTheList().get(2));
        assertEquals(expectedList, CustomerDao.customersNamesakes(CustomerList.fillTheList()));
    }

    @org.junit.Test
    public void customerWithDefinedCard() {
        assertEquals(CustomerList.fillTheList().get(4), CustomerDao.customerWithDefinedCard(CustomerList.fillTheList(), 5000));
    }

//    @Parameterized.Parameters
//    public static Collection testDataProvider() {
//        return Arrays.asList(new String[][] {
//                { new Customer(1L, "Ivanov", "Ivan", "Ivanovich", "Kharkiv", 1000, "Number 1000");
//                new Customer(2L, "Tkachenko", "Ivan", "Aleksandrovich", "Odessa", 2000, "Number 2000");
//        new Customer(3L, "Iliyin", "Iliia", "Illich", "Kiev", 3000, "Number 3000");
//        new Customer(4L, "Shevchenko", "Taras", "Anatolievich","Lviv", 4000, "Number 4000");
//        new Customer(5L, "Holovata", "Anastasia", "Olegovna", "Dnipro", 5000, "Number 5000");
//        });
//    }
}