package com.github.practice5.customerPart;

import java.util.ArrayList;
import java.util.List;

public class CustomerList {

    public static List<Customer> fillTheList() {
        List<Customer> expectedList = new ArrayList<>();

        Customer customer1 = new Customer(1L, "Ivanov", "Ivan", "Ivanovich", "Kharkiv",
                1000, "Number 1000");
        Customer customer2 = new Customer(2L, "Tkachenko", "Iliia", "Aleksandrovich", "Odessa",
                2000, "Number 2000");
        Customer customer3 = new Customer(3L, "Iliyin", "Iliia", "Illich", "Kiev",
                3000, "Number 3000");
        Customer customer4 = new Customer(4L, "Shevchenko", "Taras", "Anatolievich", "Lviv",
                4000, "Number 4000");
        Customer customer5 = new Customer(5L, "Holovata", "Anastasia", "Olegovna", "Dnipro",
                5000, "Number 5000");
        expectedList.add(customer1);
        expectedList.add(customer2);
        expectedList.add(customer3);
        expectedList.add(customer4);
        expectedList.add(customer5);

        return expectedList;
    }
}
