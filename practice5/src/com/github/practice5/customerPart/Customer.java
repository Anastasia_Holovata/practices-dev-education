package com.github.practice5.customerPart;

import java.util.Objects;

public class Customer {

    private long id;
    private String lastName;
    private String firstName;
    private String patronymic;
    private String address;
    private int numberOfCard;
    private String numberOfBankBill;

    public Customer() {
    }

    public Customer(long id, String lastName, String firstName, String patronymic, String address, int numberOfCard, String numberOfBankBill) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.address = address;
        this.numberOfCard = numberOfCard;
        this.numberOfBankBill = numberOfBankBill;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getNumberOfCard() {
        return numberOfCard;
    }

    public void setNumberOfCard(int numberOfCard) {
        this.numberOfCard = numberOfCard;
    }

    public String getNumberOfBankBill() {
        return numberOfBankBill;
    }

    public void setNumberOfBankBill(String numberOfBankBill) {
        this.numberOfBankBill = numberOfBankBill;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id &&
                numberOfCard == customer.numberOfCard &&
                Objects.equals(lastName, customer.lastName) &&
                Objects.equals(firstName, customer.firstName) &&
                Objects.equals(patronymic, customer.patronymic) &&
                Objects.equals(address, customer.address) &&
                Objects.equals(numberOfBankBill, customer.numberOfBankBill);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lastName, firstName, patronymic, address, numberOfCard, numberOfBankBill);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", address='" + address + '\'' +
                ", numberOfCard=" + numberOfCard +
                ", numberOfBankBill='" + numberOfBankBill + '\'' +
                '}';
    }
}
