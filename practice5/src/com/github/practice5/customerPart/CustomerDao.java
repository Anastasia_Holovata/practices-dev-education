package com.github.practice5.customerPart;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CustomerDao {

    public static void main(String[] args) {
        System.out.println("Customers with the same names:\n" + customersNamesakes(CustomerList.fillTheList()));
        System.out.println("\nCustomer with card 5000:\n" +
                customerWithDefinedCard(CustomerList.fillTheList(), 5000));
    }

    public static List<Customer> customersNamesakes(List<Customer> customers) {

        Set<String> store = new HashSet<>();
        List<String> names = new ArrayList<>();

        for (Customer customer : customers) {
            if (!store.add(customer.getFirstName())) {
                names.add(customer.getFirstName());
            }
        }
        List<Customer> nameSakes = new ArrayList<>();
        for (Customer customer : customers) {
            if (names.contains(customer.getFirstName())) {
                nameSakes.add(customer);
            }
        }
        return nameSakes;
    }

    public static Customer customerWithDefinedCard(List<Customer> customers, int neededCard) {
        Customer resultCustomer = null;
        for (Customer customer : customers) {
            if (customer.getNumberOfCard() == neededCard) {
                resultCustomer = customer;
            }
        }
        return resultCustomer;
    }
}
