package com.github.practice5.studentPart;

import java.util.ArrayList;
import java.util.List;

public class StudentDao {

    public static void main(String[] args) {
        System.out.println("The students of faculty Software Engineering:\n" +
                getListOfStudentsInDefinedFaculty(StudentList.fillTheList(), "Software Engineering"));
        System.out.println("The students were born after 1997:\n" +
                getListOfStudentsFromYear(StudentList.fillTheList(), 1997));
        System.out.println("The students in SE-3:\n" +
                getListOfGroup(StudentList.fillTheList(), "SE-3"));
    }

    public static List<Student> getListOfStudentsInDefinedFaculty(List<Student> students, String neededFaculty) {
        List<Student> studentsAtFaculty = new ArrayList<>();
        for (Student student : students) {
            if (student.getFaculty().equals(neededFaculty)) {
                studentsAtFaculty.add(student);
            }
        }
        return studentsAtFaculty;
    }
//TODO:
    public static List<Object> getListOfStudentsForEveryFacultyAndCourse() {
        List<Student> studentList = new ArrayList<>();
        return null;
    }

    public static List<Student> getListOfStudentsFromYear(List<Student> students, int neededYear) {
        List<Student> studentsList = new ArrayList<>();
        for (Student student : students) {
            if (student.getDateOfBirth() == neededYear || student.getDateOfBirth() > neededYear) {
                studentsList.add(student);
            }
        }
        return studentsList;
    }

    public static List<Student> getListOfGroup(List<Student> students, String neededGroup) {
        List<Student> listOfStudentsGroup = new ArrayList<>();
        for (Student student : students) {
            if (student.getGroup().equals(neededGroup)) {
                listOfStudentsGroup.add(student);
            }
        }
        return listOfStudentsGroup;
    }
}
