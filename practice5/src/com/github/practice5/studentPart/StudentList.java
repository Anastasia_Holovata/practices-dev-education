package com.github.practice5.studentPart;

import java.util.ArrayList;
import java.util.List;

public class StudentList {
    public static List<Student> fillTheList() {

        List<Student> suggestedStudents = new ArrayList<>();
        Student student1 = new Student(1, "Ivanov", "Ivan", "Ivanovich",
                1999, "Dnipro", "0961234567", "International Relationships",
                3, "IR-3-18");
        suggestedStudents.add(student1);

        Student student2 = new Student(2, "Tkachenko", "Mariia", "Aleksandrovna",
                2000, "Dnipro", "0999997653", "Software Engineering",
                2, "SE-2-19");
        suggestedStudents.add(student2);

        Student student3 = new Student(3, "Iliyin", "Iliia", "Illich",
                2001, "Dnipro", "0958794525", "Software Engineering",
                3, "SE-3");
        suggestedStudents.add(student3);

        Student student4 = new Student(4, "Shevchenko", "Taras", "Anatolievich",
                1995, "Dnipro", "0983673556", "International Relationships",
                7, "IR-1-14");
        suggestedStudents.add(student4);

        Student student5 = new Student(5, "Holovata", "Anastasia", "Olegovna",
                2002, "Dnipro", "0501239845", "Software Engineering",
                2, "SE-2-19");
        suggestedStudents.add(student5);
        return suggestedStudents;
    }
}
